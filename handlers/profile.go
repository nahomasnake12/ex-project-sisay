package handlers

import (
	"ex-project/database"
	"ex-project/models"
	"fmt"
	"io/ioutil"
	"path/filepath"

	"github.com/gin-gonic/gin"
	"github.com/google/uuid"
)

func CreateProfile(c *gin.Context) {
	pr := &models.Profile{}
	err := c.Bind(pr)
	if err != nil {
		c.JSON(400, models.ErrorRespose{Error: err.Error()})
		return
	}
	err = database.CreateProfile(pr)
	if err != nil {
		c.JSON(400, models.ErrorRespose{Error: err.Error()})
		return
	}
	c.JSON(200, models.SuccessRespose{Data: pr})

}

func GetProfile(c *gin.Context) {
	pr := &models.Profile{}
	id, err := uuid.Parse(c.Param("id"))
	if err != nil {
		c.JSON(400, models.ErrorRespose{Error: err.Error()})
		return
	}
	err = c.Bind(pr)
	if err != nil {
		c.JSON(400, models.ErrorRespose{Error: err.Error()})
		return
	}
	profile, err := database.GetProfile(&models.Profile{ID: id})
	if err != nil {
		c.JSON(400, models.ErrorRespose{Error: err.Error()})
		return
	}
	c.JSON(200, models.SuccessRespose{Data: profile})
}

func GetAllProfiles(c *gin.Context) {
	pr := []models.Profile{}
	err := c.Bind(pr)
	if err != nil {
		c.JSON(400, models.ErrorRespose{Error: err.Error()})
		return
	}
	profile, err := database.GetAllProfiles(&models.Profile{})
	if err != nil {
		c.JSON(400, models.ErrorRespose{Error: err.Error()})
		return
	}
	c.JSON(200, models.SuccessRespose{Data: profile})

}

func UpdateProfile(c *gin.Context) {
	pr := &models.Profile{}
	id, err := uuid.Parse(c.Param("id"))
	if err != nil {
		c.JSON(400, models.ErrorRespose{Error: err.Error()})
		return
	}
	err = c.Bind(pr)
	if err != nil {
		c.JSON(400, models.ErrorRespose{Error: err.Error()})
		return
	}
	err = database.UpdateProfile(&models.Profile{ID: id}, pr)
	if err != nil {
		c.JSON(400, models.ErrorRespose{Error: err.Error()})
		return
	}
	c.JSON(200, models.SuccessRespose{Data: pr})

}

func DeleteProfile(c *gin.Context) {
	pr := &models.Profile{}
	id, err := uuid.Parse(c.Param("id"))
	if err != nil {
		c.JSON(400, models.ErrorRespose{Error: err.Error()})
		return
	}
	err = c.Bind(pr)
	if err != nil {
		c.JSON(400, models.ErrorRespose{Error: err.Error()})
		return
	}
	err = database.DeleteProfile(&models.Profile{ID: id})
	if err != nil {
		c.JSON(400, models.ErrorRespose{Error: err.Error()})
		return
	}
	c.JSON(200, models.SuccessRespose{Data: id})
}

func ProfilePhoto(c *gin.Context) {

	file, handler, err := c.Request.FormFile("photo")
	if err != nil {
		fmt.Println("Error Retrieving the File")
		fmt.Println(err)
		return
	}
	defer file.Close()
	fmt.Printf("Uploaded File: %+v\n", handler.Filename)
	fmt.Printf("File Size: %+v\n", handler.Size)
	fmt.Printf("MIME Header: %+v\n", handler.Header)

	fileName := uuid.NewString() + handler.Filename
	path := filepath.Join(`http://localhost:9090`, "profile_photos")
	// Create a temporary file within our temp-images directory that follows
	// a particular naming pattern
	tempFile, err := ioutil.TempFile("profile_photos", "*"+fileName)
	if err != nil {
		fmt.Println(err)
	}
	defer tempFile.Close()

	// read all of the contents of our uploaded file into a
	// byte array
	fileBytes, err := ioutil.ReadAll(file)
	if err != nil {
		fmt.Println(err)
	}
	// write this byte array to our temporary file
	tempFile.Write(fileBytes)
	c.JSON(200, models.SuccessRespose{Data: path + "/" + tempFile.Name()})

}
