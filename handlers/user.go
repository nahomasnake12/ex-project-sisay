package handlers

import (
	"ex-project/database"
	"ex-project/models"

	"github.com/casbin/casbin/v2"
	"github.com/gin-gonic/gin"
	"github.com/google/uuid"
	"golang.org/x/crypto/bcrypt"
)

func HashPassword(password string) (string, error) {
	bytes, err := bcrypt.GenerateFromPassword([]byte(password), 14)
	return string(bytes), err
}

// func CheckPasswordHash(password, hash string) bool {
// 	err := bcrypt.CompareHashAndPassword([]byte(hash), []byte(password))
// 	return err == nil
// }

func CreateUser(enforcer *casbin.Enforcer) gin.HandlerFunc {
	return func(c *gin.Context) {
		user := &models.User{}
		err := c.Bind(user)
		if err != nil {
			c.JSON(400, models.ErrorRespose{Error: err.Error()})
			return
		}
		encpass, err := HashPassword(user.Password)
		if err != nil {
			c.JSON(401, models.ErrorRespose{Error: err.Error()})
			return
		}
		user.Password = string(encpass)
		err = database.CreateUser(user)
		if err != nil {
			c.JSON(400, models.ErrorRespose{Error: err.Error()})
			return
		}
		c.JSON(200, models.SuccessRespose{Data: user})
	}
}

func GetUser(c *gin.Context) {
	user := &models.User{}
	find, err := uuid.Parse(c.Param("id"))
	if err != nil {
		c.JSON(400, models.ErrorRespose{Error: err.Error()})
		return
	}
	err = c.Bind(user)
	if err != nil {
		c.JSON(400, models.ErrorRespose{Error: err.Error()})
		return
	}
	ur, err := database.GetUser(&models.User{ID: find})
	if err != nil {
		c.JSON(400, models.ErrorRespose{Error: err.Error()})
		return
	}
	c.JSON(200, models.SuccessRespose{Data: ur})

}

func GetAllUser(c *gin.Context) {
	user := []models.User{}
	err := c.Bind(user)
	if err != nil {
		c.JSON(400, models.ErrorRespose{Error: err.Error()})
		return
	}
	ur, err := database.GetAllUsers(&models.User{})
	if err != nil {
		c.JSON(400, models.ErrorRespose{Error: err.Error()})
		return
	}
	c.JSON(200, models.SuccessRespose{Data: ur})

}

func UpdateUser(c *gin.Context) {
	user := &models.User{}
	find, err := uuid.Parse(c.Param("id"))
	if err != nil {
		c.JSON(400, models.ErrorRespose{Error: err.Error()})
		return
	}
	err = c.Bind(user)
	if err != nil {
		c.JSON(400, models.ErrorRespose{Error: err.Error()})
		return
	}
	err = database.UpdateUser(&models.User{ID: find}, user)
	if err != nil {
		c.JSON(400, models.ErrorRespose{Error: err.Error()})
		return
	}
	c.JSON(200, models.SuccessRespose{Data: user})
}

func DeleteUser(c *gin.Context) {
	user := &models.User{}
	id, err := uuid.Parse(c.Param("id"))
	if err != nil {
		c.JSON(400, models.ErrorRespose{Error: err.Error()})
		return
	}
	err = c.Bind(user)
	if err != nil {
		c.JSON(400, models.ErrorRespose{Error: err.Error()})
		return
	}
	err = database.DeleteUser(&models.User{ID: id})
	if err != nil {
		c.JSON(400, models.ErrorRespose{Error: err.Error()})
		return
	}
	c.JSON(200, models.SuccessRespose{Data: id})

}
