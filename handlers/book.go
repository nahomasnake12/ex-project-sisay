package handlers

import (
	"ex-project/database"
	"ex-project/models"
	"fmt"
	"io/ioutil"
	"path/filepath"

	"github.com/gin-gonic/gin"
	"github.com/google/uuid"
)

func CreateBook(c *gin.Context) {
	book := &models.Book{}
	err := c.Bind(book)
	if err != nil {
		c.JSON(400, models.ErrorRespose{Error: err.Error()})
		return
	}

	err = database.CreateBook(book)
	if err != nil {
		c.JSON(400, models.ErrorRespose{Error: err.Error()})
		return
	}
	c.JSON(200, models.SuccessRespose{Data: book})
}

func UploadImage(c *gin.Context) {

	file, handler, err := c.Request.FormFile("cover")
	if err != nil {
		fmt.Println("Error Retrieving the File")
		fmt.Println(err)
		return
	}
	defer file.Close()
	fmt.Printf("Uploaded File: %+v\n", handler.Filename)
	fmt.Printf("File Size: %+v\n", handler.Size)
	fmt.Printf("MIME Header: %+v\n", handler.Header)

	fileName := uuid.NewString() + handler.Filename
	path := filepath.Join(`http://localhost:9090`, "profile_photos")
	// Create a temporary file within our temp-images directory that follows
	// a particular naming pattern
	tempFile, err := ioutil.TempFile("profile_photos", "*"+fileName)
	if err != nil {
		fmt.Println(err)
	}
	defer tempFile.Close()

	// read all of the contents of our uploaded file into a
	// byte array
	fileBytes, err := ioutil.ReadAll(file)
	if err != nil {
		fmt.Println(err)
	}
	// write this byte array to our temporary file
	tempFile.Write(fileBytes)
	c.JSON(200, models.SuccessRespose{Data: path + "/" + tempFile.Name()})

}

func UpdateBook(c *gin.Context) {
	book := &models.Book{}
	id, err := uuid.Parse(c.Param("id"))
	if err != nil {
		c.JSON(400, models.ErrorRespose{Error: err.Error()})
		return
	}

	err = c.Bind(book)
	if err != nil {
		c.JSON(400, models.ErrorRespose{Error: err.Error()})
		return
	}
	err = database.UpdateBook(&models.Book{ID: id}, book)
	if err != nil {
		c.JSON(400, models.ErrorRespose{Error: err.Error()})
		return
	}
	c.JSON(200, models.SuccessRespose{Data: book})

}

func GetBook(c *gin.Context) {
	book := &models.Book{}
	find, err := uuid.Parse(c.Param("id"))
	if err != nil {
		c.JSON(400, models.ErrorRespose{Error: err.Error()})
		return
	}
	err = c.Bind(book)
	if err != nil {
		c.JSON(400, models.ErrorRespose{Error: err.Error()})
		return
	}
	bk, err := database.GetBook(&models.Book{ID: find})
	if err != nil {
		c.JSON(400, models.ErrorRespose{Error: err.Error()})
		return
	}

	c.JSON(200, models.SuccessRespose{Data: bk})

}

func GetBooks(c *gin.Context) {
	book := []models.Book{}
	err := c.Bind(book)
	if err != nil {
		c.JSON(400, models.ErrorRespose{Error: err.Error()})
		return
	}
	bk, err := database.GetBooks(&models.Book{})
	if err != nil {
		c.JSON(400, models.ErrorRespose{Error: err.Error()})
		return
	}

	c.JSON(200, models.SuccessRespose{Data: bk})

}

func DeleteBook(c *gin.Context) {
	book := &models.Book{}
	id, err := uuid.Parse(c.Param("id"))
	if err != nil {
		c.JSON(400, models.ErrorRespose{Error: err.Error()})
		return
	}

	err = c.Bind(book)
	if err != nil {
		c.JSON(400, models.ErrorRespose{Error: err.Error()})
		return
	}
	err = database.DeleteBook(&models.Book{ID: id})
	if err != nil {
		c.JSON(400, models.ErrorRespose{Error: err.Error()})
		return
	}
	c.JSON(200, models.SuccessRespose{Data: id})

}

// func CoverFile(c *gin.Context) {
// 	// Source
// 	file, err := c.FormFile("file")
// 	if err != nil {
// 		c.String(http.StatusBadRequest, "get form err: %s", err.Error())
// 		return
// 	}
// 	filename := filepath.Base(file.Filename)
// 	if err := c.SaveUploadedFile(file, filename); err != nil {
// 		c.String(http.StatusBadRequest, "upload file err: %s", err.Error())
// 		return
// 	}
// 	c.String(http.StatusOK, "File %s uploaded successfully with fields name=%s and email=%s.", file.Filename)

// }
