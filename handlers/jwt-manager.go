package handlers

import (
	"ex-project/models"

	"github.com/golang-jwt/jwt"
)

type JWTManager struct {
	secretKey string
}

func NewJWTManager(secretKey string) *JWTManager {
	return &JWTManager{secretKey}
}

func (manager *JWTManager) Generate(userClaims *models.UserClaims) (string, error) {

	token := jwt.NewWithClaims(jwt.SigningMethodHS256, userClaims)
	tk, err := token.SignedString([]byte(manager.secretKey))
	if err != nil {
		return "", err
	}
	return tk, nil
}

func (manager *JWTManager) Verify(accessToken string) (*models.UserClaims, error) {
	if accessToken == "" {
		return nil, nil
	}
	token, err := jwt.ParseWithClaims(
		accessToken,
		&models.UserClaims{},
		func(token *jwt.Token) (interface{}, error) {
			_, ok := token.Method.(*jwt.SigningMethodHMAC)
			if !ok {
				return nil, nil
			}

			return []byte(manager.secretKey), nil
		},
	)

	if err != nil {
		return nil, err
	}

	claims, ok := token.Claims.(*models.UserClaims)
	if !ok {
		return nil, nil
	}

	return claims, nil
}
