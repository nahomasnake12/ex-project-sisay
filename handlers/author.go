package handlers

import (
	"ex-project/database"
	"ex-project/models"

	"github.com/gin-gonic/gin"
	"github.com/google/uuid"
)

func AddAuthor(c *gin.Context) {
	author := &models.Author{}
	err := c.Bind(author)
	if err != nil {
		c.JSON(400, models.ErrorRespose{Error: err.Error()})
		return
	}

	err = database.AddAuthor(author)
	if err != nil {
		c.JSON(400, models.ErrorRespose{Error: err.Error()})
		return
	}
	c.JSON(200, models.SuccessRespose{Data: author})

}

func GetAuthor(c *gin.Context) {
	author := &models.Author{}
	find, err := uuid.Parse(c.Param("id"))
	if err != nil {
		c.JSON(400, models.ErrorRespose{Error: err.Error()})
		return
	}
	err = c.Bind(author)
	if err != nil {
		c.JSON(400, models.ErrorRespose{Error: err.Error()})
		return
	}
	at, err := database.GetAuthor(&models.Author{ID: find})
	if err != nil {
		c.JSON(400, models.ErrorRespose{Error: err.Error()})
		return
	}
	c.JSON(200, models.SuccessRespose{Data: at})
}

func GetAllAuthor(c *gin.Context) {
	author := []models.Author{}
	err := c.Bind(author)
	if err != nil {
		c.JSON(400, models.ErrorRespose{Error: err.Error()})
		return
	}
	at, err := database.GetAllAuthor(&models.Author{})
	if err != nil {
		c.JSON(400, models.ErrorRespose{Error: err.Error()})
		return
	}
	c.JSON(200, models.SuccessRespose{Data: at})
}

func UpdateAuthor(c *gin.Context) {
	author := &models.Author{}
	id, err := uuid.Parse(c.Param("id"))
	if err != nil {
		c.JSON(400, models.ErrorRespose{Error: err.Error()})
		return
	}

	err = c.Bind(author)
	if err != nil {
		c.JSON(400, models.ErrorRespose{Error: err.Error()})
		return
	}

	err = database.UpdateAuthor(&models.Author{ID: id}, author)
	if err != nil {
		c.JSON(400, models.ErrorRespose{Error: err.Error()})
		return
	}
	c.JSON(200, models.SuccessRespose{Data: author})
}

func DeleteAuthor(c *gin.Context) {
	author := &models.Author{}
	id, err := uuid.Parse(c.Param("id"))
	if err != nil {
		c.JSON(400, models.ErrorRespose{Error: err.Error()})
		return
	}
	err = c.Bind(author)
	if err != nil {
		c.JSON(400, models.ErrorRespose{Error: err.Error()})
		return
	}
	err = database.DeleteAuthor(&models.Author{ID: id})
	if err != nil {
		c.JSON(400, models.ErrorRespose{Error: err.Error()})
		return
	}
	c.JSON(200, models.SuccessRespose{Data: id})

}
