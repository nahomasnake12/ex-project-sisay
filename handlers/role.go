package handlers

import (
	"ex-project/database"
	"ex-project/models"

	"github.com/gin-gonic/gin"
	"github.com/google/uuid"
)

func AddRole(c *gin.Context) {
	r := &models.Role{}
	err := c.Bind(r)
	if err != nil {
		c.JSON(400, models.ErrorRespose{Error: err.Error()})
		return
	}
	err = database.AddRole(r)
	if err != nil {
		c.JSON(400, models.ErrorRespose{Error: err.Error()})
		return
	}
	c.JSON(200, models.SuccessRespose{Data: r})
}

func GetRole(c *gin.Context) {
	r := &models.Role{}
	id, err := uuid.Parse(c.Param("id"))
	if err != nil {
		c.JSON(400, models.ErrorRespose{Error: err.Error()})
		return
	}
	err = c.Bind(r)
	if err != nil {
		c.JSON(400, models.ErrorRespose{Error: err.Error()})
		return
	}
	rl, err := database.GetRole(&models.Role{ID: id})
	if err != nil {
		c.JSON(400, models.ErrorRespose{Error: err.Error()})
		return
	}
	c.JSON(200, models.SuccessRespose{Data: rl})
}

func GetAllRoles(c *gin.Context) {
	r := []models.Role{}
	err := c.Bind(r)
	if err != nil {
		c.JSON(400, models.ErrorRespose{Error: err.Error()})
		return
	}
	rl, err := database.GetAllRoles(&models.Role{})
	if err != nil {
		c.JSON(400, models.ErrorRespose{Error: err.Error()})
		return
	}
	c.JSON(200, models.SuccessRespose{Data: rl})
}

func UpdateRole(c *gin.Context) {
	r := &models.Role{}
	id, err := uuid.Parse(c.Param("id"))
	if err != nil {
		c.JSON(400, models.ErrorRespose{Error: err.Error()})
		return
	}
	err = c.Bind(r)
	if err != nil {
		c.JSON(400, models.ErrorRespose{Error: err.Error()})
		return
	}
	err = database.UpdateRole(&models.Role{ID: id}, r)
	if err != nil {
		c.JSON(400, models.ErrorRespose{Error: err.Error()})
		return
	}
	c.JSON(200, models.SuccessRespose{Data: r})
}

func DeleteRole(c *gin.Context) {
	r := &models.Role{}
	id, err := uuid.Parse(c.Param("id"))
	if err != nil {
		c.JSON(400, models.ErrorRespose{Error: err.Error()})
		return
	}
	err = c.Bind(r)
	if err != nil {
		c.JSON(400, models.ErrorRespose{Error: err.Error()})
		return
	}
	err = database.DeleteRoles(&models.Role{ID: id})
	if err != nil {
		c.JSON(400, models.ErrorRespose{Error: err.Error()})
		return
	}
	c.JSON(200, models.SuccessRespose{Data: id})
}

func AsignUserRole(c *gin.Context) {
	uid, err := uuid.Parse(c.Param("user-id"))
	if err != nil {
		c.JSON(400, models.ErrorRespose{Error: err.Error()})
		return
	}
	rid, err := uuid.Parse(c.Param("role-id"))
	if err != nil {
		c.JSON(400, models.ErrorRespose{Error: err.Error()})
		return
	}
	u := &models.User{ID: uid}
	r := &models.Role{ID: rid}
	ur := &models.UserRole{}
	err = c.Bind(ur)
	if err != nil {
		c.JSON(400, models.ErrorRespose{Error: err.Error()})
		return
	}
	urs, err := database.AsignUserRole(u, r)
	if err != nil {
		c.JSON(400, models.ErrorRespose{Error: err.Error()})
		return
	}
	c.JSON(200, models.SuccessRespose{Data: urs})

}

// func GetUserRole(c *gin.Context) {

// }
