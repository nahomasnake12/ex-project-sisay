package handlers

import (
	"ex-project/auth"
	"ex-project/database"
	"ex-project/models"

	"github.com/gin-gonic/gin"
)

func Register(c *gin.Context) {
	input := models.User{}
	err := c.ShouldBindJSON(&input)
	if err != nil {
		c.JSON(400, models.ErrorRespose{Error: err.Error()})
		return
	}

	err = database.CreateUser(&input)
	if err != nil {
		c.JSON(400, models.ErrorRespose{Error: err.Error()})
		return
	}
	// user := &models.User{}
	c.JSON(200, models.SuccessRespose{Data: "validated!"})
}

func Login(c *gin.Context) {
	input := models.User{}
	err := c.Bind(&input)
	if err != nil {
		c.JSON(400, models.ErrorRespose{Error: err.Error()})
		return
	}
	token, err := database.LoginCheck(input.Email, input.Password)
	if err != nil {
		c.JSON(400, models.ErrorRespose{Error: err.Error()})
		return
	}
	c.JSON(200, models.SuccessRespose{Data: token})
}

// func Logout(c *gin.Context) {
// 	metadata, _ := auth.ExtractTokenID(c)
// 	if metadata != nil {
// 		deleteErr := servers.HttpServer.RD.DeleteTokens(metadata)
// 		if deleteErr != nil {
// 			c.JSON(http.StatusBadRequest, deleteErr.Error())
// 			return
// 		}
// 	}
// 	c.JSON(http.StatusOK, "Successfully logged out")
// }

func CurrentUser(c *gin.Context) {
	user_id, err := auth.ExtractTokenID(c)
	if err != nil {
		c.JSON(400, models.ErrorRespose{Error: err.Error()})
		return
	}

	u, err := database.GetUserByID(user_id)
	if err != nil {
		c.JSON(400, models.ErrorRespose{Error: err.Error()})
		return
	}
	c.JSON(200, models.SuccessRespose{Data: u})
}
