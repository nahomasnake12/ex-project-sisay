package database

import "ex-project/models"

func AddAuthor(author *models.Author) error {
	conn := DB()
	err := conn.Create(author).Error
	if err != nil {
		return err
	}
	return nil
}

func GetAuthor(params *models.Author) (*models.Author, error) {
	conn := DB()
	writer := &models.Author{}
	err := conn.Where(params).First(writer).Error
	if err != nil {
		return nil, err
	}
	return writer, err
}

func GetAllAuthor(params *models.Author) ([]models.Author, error) {
	conn := DB()
	writer := []models.Author{}
	err := conn.Where(params).Find(&writer).Error
	if err != nil {
		return nil, err
	}
	return writer, nil
}

func UpdateAuthor(params *models.Author, writer *models.Author) error {
	conn := DB()
	err := conn.Where(params).Updates(writer).Error
	if err != nil {
		return nil
	}
	return nil
}

func DeleteAuthor(writer *models.Author) error {
	conn := DB()
	err := conn.Where("id=?", writer.ID).Delete(writer).Error
	if err != nil {
		return nil
	}
	return nil
}
