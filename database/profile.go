package database

import (
	"ex-project/models"
)

func CreateProfile(pr *models.Profile) error {
	conn := DB()
	err := conn.Create(pr).Error
	if err != nil {
		return err
	}
	return nil
}

func GetProfile(params *models.Profile) (*models.Profile, error) {
	conn := DB()
	pr := &models.Profile{}
	err := conn.Where(params).First(pr).Error
	if err != nil {
		return nil, err
	}
	return pr, nil
}

func GetAllProfiles(Params *models.Profile) ([]models.Profile, error) {
	conn := DB()
	pr := []models.Profile{}
	err := conn.Where(Params).Find(pr).Error
	if err != nil {
		return nil, err
	}
	return pr, nil
}

func UpdateProfile(params *models.Profile, pr *models.Profile) error {
	conn := DB()
	err := conn.Where(params).Updates(pr).Error
	if err != nil {
		return err
	}
	return nil
}

func DeleteProfile(pr *models.Profile) error {
	conn := DB()
	err := conn.Where("id=?", pr.ID).Delete(pr).Error
	if err != nil {
		return err
	}
	return nil
}
