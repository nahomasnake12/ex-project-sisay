package database

import "ex-project/models"

func CreateBook(book *models.Book) error {
	conn := DB()

	err := conn.Create(book).Error
	if err != nil {
		return err
	}
	return nil
}

func UpdateBook(params *models.Book, book *models.Book) error {
	conn := DB()

	err := conn.Where(params).Updates(book).Error
	if err != nil {
		return err
	}
	return nil
}

func GetBook(params *models.Book) (*models.Book, error) {
	conn := DB()
	book := &models.Book{}
	err := conn.Where(params).First(book).Error
	if err != nil {
		return nil, err
	}
	return book, nil
}

func GetBooks(params *models.Book) ([]models.Book, error) {
	conn := DB()
	books := []models.Book{}
	err := conn.Where(params).Find(&books).Error
	if err != nil {
		return nil, err
	}
	return books, nil
}

func DeleteBook(book *models.Book) error {
	conn := DB()

	err := conn.Where("id=?", book.ID).Delete(book).Error
	if err != nil {
		return err
	}
	return nil
}

// func SaveFile(image *models.Book) {

// }
