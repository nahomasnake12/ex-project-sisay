package database

import (
	"ex-project/models"
)

func AddRole(r *models.Role) error {
	conn := DB()
	err := conn.Create(r).Error
	if err != nil {
		return err
	}
	return nil
}

func GetRole(params *models.Role) (*models.Role, error) {
	conn := DB()
	r := &models.Role{}
	err := conn.Where(params).First(r).Error
	if err != nil {
		return nil, err
	}
	return r, nil
}

func GetAllRoles(params *models.Role) ([]models.Role, error) {
	conn := DB()
	r := []models.Role{}
	err := conn.Where(params).Find(r).Error
	if err != nil {
		return nil, err
	}
	return r, nil
}

func UpdateRole(params *models.Role, r *models.Role) error {
	conn := DB()
	err := conn.Where(params).Updates(r).Error
	if err != nil {
		return err
	}
	return nil
}

func DeleteRoles(r *models.Role) error {
	conn := DB()
	err := conn.Where("id=?", r.ID).Delete(r).Error
	if err != nil {
		return err
	}
	return nil
}

func AsignUserRole(u *models.User, r *models.Role) (*models.UserRole, error) {
	conn := DB()
	ur := &models.UserRole{UserID: u.ID, RoleID: r.ID}
	err := conn.Where(&models.UserRole{}).Create(ur).Error
	if err != nil {
		return nil, err
	}
	return ur, nil
}

func GetUserRole(params *models.User) (*models.UserRole, error) {
	conn := DB()
	ur := &models.UserRole{UserID: params.ID}
	err := conn.Where(&models.UserRole{}).First(ur).Error
	if err != nil {
		return nil, err
	}
	return ur, nil
}
