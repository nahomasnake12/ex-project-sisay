package database

import (
	"ex-project/models"
	"fmt"
	"log"

	"gorm.io/driver/postgres"
	"gorm.io/gorm"
	"gorm.io/gorm/logger"
)

func DB() *gorm.DB {

	dsn := "host=localhost user=postgres password=example dbname=postgres port=5432 sslmode=disable TimeZone=Asia/Shanghai"
	db, err := gorm.Open(postgres.Open(dsn), &gorm.Config{
		Logger: logger.Default,
	})
	if err != nil {
		log.Printf("error during opening a connection to db. %v", err.Error())
	}

	err = db.AutoMigrate(&models.Book{}, &models.Author{}, &models.User{}, &models.Profile{}, &models.UserRole{}, &models.CasbinRule{})
	if err != nil {
		fmt.Println(err.Error())
	}
	log.Printf("database connected successfully.\n")
	return db
}
