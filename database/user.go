package database

import (
	"errors"
	"ex-project/models"
	"strings"

	// "github.com/badoux/checkmail"
	"github.com/badoux/checkmail"
	"github.com/google/uuid"
)

func Validate(user *models.User, action string) error {
	switch strings.ToLower(action) {
	case "login":

		if user.Password == "" {
			return errors.New("required password")
		}
		if user.Email == "" {
			return errors.New("required email")
		}
		if err := checkmail.ValidateFormat(user.Email); err != nil {
			return errors.New("invalid email")
		}

		return nil

	default:
		if user.Password == "" {
			return errors.New("required eassword")
		}
		if user.Email == "" {
			return errors.New("required email")
		}
		if err := checkmail.ValidateFormat(user.Email); err != nil {
			return errors.New("invalid email")
		}
		return nil

	}

}

func CreateUser(user *models.User) error {
	conn := DB()

	err := conn.Create(user).Error
	if err != nil {
		return err
	}
	return nil
}

func GetUser(params *models.User) (*models.User, error) {
	conn := DB()
	ur := &models.User{}
	//preload is used for fech the user role from role table
	err := conn.Where(params).Preload("Roles").First(ur).Error
	if err != nil {
		return nil, err
	}
	return ur, nil
}

func GetUserByID(uid uuid.UUID) (*models.User, error) {
	conn := DB()
	u := &models.User{}

	err := conn.First(&u, uid).Error
	if err != nil {
		return u, errors.New("User not found!")
	}

	u.PrepareGive()

	return u, nil
}

func GetAllUsers(params *models.User) ([]models.User, error) {
	conn := DB()
	ur := &[]models.User{}
	err := conn.Where(params).Preload("Roles").Find(ur).Error
	if err != nil {
		return nil, err
	}
	return *ur, nil
}

func UpdateUser(params *models.User, user *models.User) error {
	conn := DB()
	err := conn.Where(params).Updates(user).Error
	if err != nil {
		return err
	}
	return nil
}

func DeleteUser(user *models.User) error {
	conn := DB()
	err := conn.Where("id=?", user.ID).Delete(user).Error
	if err != nil {
		return err
	}
	return nil
}
