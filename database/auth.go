package database

import (
	"ex-project/auth"
	"ex-project/models"

	"golang.org/x/crypto/bcrypt"
)

func VerifyPassword(password, hashedPassword string) error {
	return bcrypt.CompareHashAndPassword([]byte(hashedPassword), []byte(password))
}

// func (u *models.User) PrepareGive() {
// 	u.Password = ""
// }

func LoginCheck(email string, password string) (string, error) {
	conn := DB()
	var err error

	u := &models.User{}

	err = conn.Model(u).Where("email = ?", email).Take(&u).Error

	if err != nil {
		return "", err
	}
	err = VerifyPassword(password, u.Password)

	if err != nil && err == bcrypt.ErrMismatchedHashAndPassword {
		return "", err
	}

	token, err := auth.GenerateToken(u.ID)
	if err != nil {
		return "", err
	}

	return token, nil

}
