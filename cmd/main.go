package main

import (
	"ex-project/database"
	"ex-project/routes"
	"fmt"
	"log"
	"os"

	"github.com/gin-gonic/gin"
)

func main() {
	fmt.Println("test")
	pg := database.DB()
	if pg == nil {
		log.Printf("Failed to connect to database. \n")
		os.Exit(100)
	}

	router := gin.Default()
	routes.BookRoutes(router)

	router.Run(":9090")
}
