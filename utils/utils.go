package utils

import (
	"log"

	"github.com/casbin/casbin/v2"
	gormadapter "github.com/casbin/gorm-adapter/v3"
	"gorm.io/gorm"
)

func NewEnforcer(conn *gorm.DB, model string) *casbin.Enforcer {
	adapter, err := gormadapter.NewAdapterByDB(conn)
	if err != nil {
		log.Fatal("*errors.ErrorModel ", err)
	}

	enforcer, err := casbin.NewEnforcer("config/rbac_model.conf", adapter)
	if err != nil {
		log.Fatal("*errors.ErrorModel ", err)
	}

	enforcer.EnableAutoSave(true)
	enforcer.LoadPolicy()
	return enforcer
}
