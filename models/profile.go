package models

import (
	"mime/multipart"
	"time"

	"github.com/google/uuid"
	"gorm.io/gorm"
)

type Profile struct {
	ID           uuid.UUID `json:"id,omitempty" gorm:"type:uuid;default:uuid_generate_v4()"`
	Name         string    `json:"name"`
	Sex          string    `json:"sex"`
	BirthDate    time.Time `json:"birth_date"`
	ProfilePhoto string    `json:"profile_photo"`

	Photo *multipart.FileHeader `json:"-" gorm:"-" form:"photo"`

	CreatedAt time.Time      `json:"created_at"`
	UpdatedAt time.Time      `json:"updated_at"`
	DeletedAt gorm.DeletedAt `json:"-" gorm:"index"`
}
