package models

import (
	"encoding/json"
	"fmt"
	"net/http"
)

type ErrorRespose struct {
	Error string `json:"error"`
}

type SuccessRespose struct {
	Data interface{} `Json:"data"`
}

func JSON(w http.ResponseWriter, statusCode int, data interface{}) {
	w.WriteHeader(statusCode)
	err := json.NewEncoder(w).Encode(data)
	if err != nil {
		fmt.Fprintf(w, "%s", err.Error())
	}
}
