package models

import (
	"mime/multipart"
	"time"

	uuid "github.com/google/uuid"
	"gorm.io/gorm"
)

type Book struct {
	ID         uuid.UUID `json:"id,omitempty" gorm:"type:uuid;default:uuid_generate_v4()"`
	Name       string    `json:"name" form:"name"`
	IsdnNumber string    `json:"isdn_number" form:"isdn_number"`
	Cover      string    `json:"cover_url"`

	CoverFile *multipart.FileHeader `json:"-" gorm:"-" form:"cover"`

	CreatedAt time.Time      `json:"created_at"`
	UpdatedAt time.Time      `json:"updated_at"`
	DeletedAt gorm.DeletedAt `json:"-" gorm:"index"`
}

// func (p *Book) SaveBook(db *gorm.DB) (*Book, error) {
// 	err := db.Debug().Model(&Book{}).Create(&p).Error
// 	if err != nil {
// 		return &Book{}, err
// 	}
// 	return p, nil
// }
