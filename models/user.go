package models

import (
	"time"

	"github.com/google/uuid"
	"gorm.io/gorm"
)

type User struct {
	ID        uuid.UUID `json:"id,omitempty" gorm:"type:uuid;default:uuid_generate_v4()"`
	ProfileID uuid.UUID `json:"profile_id" gorm:"type:uuid"`
	Profile   *Profile  `json:"profile"`
	FirstName string    `json:"first_name"`
	LastName  string    `json:"last_name"`
	Email     string    `json:"email" gorm:"unique"`
	Password  string    `json:"password" validate:"required,min=8" gorm:"size:100;"`
	Phone     string    `json:"phone"`
	Roles     *[]Role   `json:"roles" gorm:"many2many:user_roles"`

	CreatedAt time.Time      `json:"created_at"`
	UpdatedAt time.Time      `json:"updated_at"`
	DeletedAt gorm.DeletedAt `json:"-" gorm:"index"`
}

func (u *User) PrepareGive() {
	u.Password = ""
}
