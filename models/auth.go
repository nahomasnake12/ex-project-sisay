package models

import (
	"github.com/golang-jwt/jwt"
	"github.com/google/uuid"
)

type UserClaims struct {
	jwt.StandardClaims
	Emial  string    `json:"email"`
	Role   string    `json:"role"`
	UserID uuid.UUID `json:"user_id,omitempty"`
}

type LoginResponse struct {
	Token  string    `json:"token"`
	Name   string    `json:"name"`
	Email  string    `json:"email"`
	UserId uuid.UUID `json:"user_id"`
	Roles  []Role    `json:"roles"`
}
