FROM golang:alpine AS builder
WORKDIR /
ADD . .
RUN go build -o bin/cmd /cmd/main.go

FROM alpine
WORKDIR /
COPY --from=builder /bin/cmd .
ENTRYPOINT [ "./cmd" ]
