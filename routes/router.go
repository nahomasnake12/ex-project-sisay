package routes

import (
	"ex-project/handlers"
	"ex-project/middlewares"

	"github.com/casbin/casbin/v2"
	"github.com/gin-gonic/gin"
)

func BookRoutes(route gin.IRouter) {
	// BOOK routing
	route.POST("/api/v1/books", handlers.CreateBook)
	route.POST("/api/v1/upload", handlers.UploadImage)
	route.Static("/images", "./images")

	route.PATCH("/api/v1/:id", handlers.UpdateBook)
	route.GET("/api/v1/:id", handlers.GetBook)
	route.GET("/api/v1/books", handlers.GetBooks)
	route.DELETE("/api/v1/:id", handlers.DeleteBook)

	//Author Routing`	o\];  bbh vtf55gfv vcfr454cxzsaw32zzq`
	route.POST("/api/v1/authors", handlers.AddAuthor)
	route.GET("/api/v1/authors/:id", handlers.GetAuthor)
	route.GET("/api/v1/authors", handlers.GetAllAuthor)
	route.PATCH("/api/v1/authors/:id", handlers.UpdateAuthor)
	route.DELETE("/api/v1/authors/:id", handlers.DeleteAuthor)

	//Users routing
	// route.POST("/api/v1/users", handlers.CreateUser)
	route.GET("/api/v1/users/:id", handlers.GetUser)
	route.GET("/api/v1/users", handlers.GetAllUser)
	route.PATCH("/api/v1/users/:id", handlers.UpdateUser)
	route.DELETE("/api/v1/users/:id", handlers.DeleteUser)

	//Profile routing
	route.POST("/api/v1/profiles", handlers.CreateProfile)
	route.GET("/api/v1/profiles/:id", handlers.GetProfile)
	route.GET("/api/v1/profiles", handlers.GetAllProfiles)
	route.PATCH("/api/v1/profiles/:id", handlers.UpdateProfile)
	route.DELETE("/api/v1/profiles/:id", handlers.DeleteProfile)

	route.POST("/api/v1/profiles/upload", handlers.ProfilePhoto)
	route.Static("/profile_photos", "./profile_photos")

	//role routing
	route.POST("/api/v1/roles", handlers.AddRole)
	route.GET("/api/v1/roles/:id", handlers.GetRole)
	route.GET("/api/v1/roles", handlers.GetAllUser)
	route.PATCH("/api/v1/roles/:id", handlers.UpdateRole)
	route.DELETE("/api/v1/roles/:id", handlers.DeleteRole)
	route.POST("/api/v1/roles/:user-id/assignuserrole/:role-id", handlers.AsignUserRole)

	route.POST("/api/v1/login", handlers.Login)
	route.POST("/api/v1/register", handlers.Register)

	authorized := route.Group("/api/v2/admin")
	authorized.Use(middlewares.JwtAuthMiddleware())
	authorized.GET("/user", handlers.CurrentUser)
	authorized.POST("/api/v1/users", handlers.CreateUser(&casbin.Enforcer{}))

}
